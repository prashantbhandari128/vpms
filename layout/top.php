<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-----------------------------------[Favicon]----------------------------------------->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-32x32.png">
    <link rel="manifest" href="assets/favicon/site.webmanifest">
    <!------------------------------------------------------------------------------------->

    <!--------------------------[Bootstrap CSS]---------------------------->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <!-------------------------[Fontawesome CSS]--------------------------->
    <link rel="stylesheet" href="assets/fontawesome-pro/css/all.css">
    <!----------------------------[Custom CSS]----------------------------->
    <link rel="stylesheet" href="assets/custom/css/site.css">
    
    <!-- <script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
    </script> -->
    <title>VPMS - <?php echo $title; ?></title>
</head>

<body class="background">